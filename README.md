![Sample Image](https://gitlab.com/edumqssan/archlinux-backgrounds/-/raw/master/backgrounds/backgrounds-grid.png)

# Archlinux Backgrounds

Backgrounds for the Arch Linux O.S.

## List: 

1. [Paper Grid](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-paper_grid.jpg)

2. [Paper Mountain Light](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-paper_mountain-light.jpg) & [Paper Mountain Dark](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-paper_mountain-darker.jpg)

4. [Rolling Release Universe 2](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-rolling_release_universe-2.jpg)

5. [Simple Fade Grid](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-simple_fade-grid.jpg)

6. [Simple Shiftzed Light](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-simple_shiftzed-light.jpg) & [Simple Shiftzed Darker](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/arch_linux-simple_shiftzed-darker.jpg)

8. [Red GNU Darker Shiftzed](https://gitlab.com/edumqssan/archlinux-backgrounds/-/blob/master/backgrounds/red_gnu-darker-shiftzed.jpg)

### License:
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)